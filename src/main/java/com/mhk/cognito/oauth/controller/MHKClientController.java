package com.mhk.cognito.oauth.controller;

import com.mhk.cognito.oauth.dto.MessageDTO;
import java.security.Principal;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/client")
@PreAuthorize("hasRole('mhkclients') or hasRole('MHKAdmins')")
public class MHKClientController {

  @GetMapping("/message")
  public ResponseEntity<MessageDTO> message(Principal principal) {
    return ResponseEntity.ok(
        new MessageDTO("Client or Admin message for " + principal.getName()));
  }
}
